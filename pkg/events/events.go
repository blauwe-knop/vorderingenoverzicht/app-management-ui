package events

var (
	AMUI_1 = NewEvent(
		"amui_1",
		"started listening",
		Low,
	)
	AMUI_2 = NewEvent(
		"amui_2",
		"server closed",
		High,
	)
	AMUI_3 = NewEvent(
		"amui_3",
		"received request to load login page",
		Low,
	)
	AMUI_4 = NewEvent(
		"amui_4",
		"no registrationToken provided",
		High,
	)
	AMUI_5 = NewEvent(
		"amui_5",
		"failed to parse template",
		High,
	)
	AMUI_6 = NewEvent(
		"amui_6",
		"failed to serve template",
		High,
	)
	AMUI_7 = NewEvent(
		"amui_7",
		"loaded login page",
		Low,
	)
	AMUI_8 = NewEvent(
		"amui_8",
		"received request to load activate page",
		Low,
	)
	AMUI_9 = NewEvent(
		"amui_9",
		"no bsn provided",
		High,
	)
	AMUI_10 = NewEvent(
		"amui_10",
		"failed to fetch registrationToken cookie",
		High,
	)
	AMUI_11 = NewEvent(
		"amui_11",
		"failed to link user identity",
		High,
	)
	AMUI_12 = NewEvent(
		"amui_12",
		"failed to parse template",
		High,
	)
	AMUI_13 = NewEvent(
		"amui_13",
		"failed to find user",
		High,
	)
	AMUI_14 = NewEvent(
		"amui_14",
		"failed to serve template",
		High,
	)
	AMUI_15 = NewEvent(
		"amui_15",
		"loaded activation page",
		Low,
	)
	AMUI_16 = NewEvent(
		"amui_16",
		"no bsn cookie found",
		Low,
	)
	AMUI_17 = NewEvent(
		"amui_17",
		"failed to parse JWT",
		High,
	)
	AMUI_18 = NewEvent(
		"amui_18",
		"no JWT Token supplied",
		High,
	)
)
