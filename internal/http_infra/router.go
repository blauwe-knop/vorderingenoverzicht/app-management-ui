// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"context"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-ui/internal/http_infra/metrics"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/pkg/repositories"
)

type key int

const (
	htmlTemplateDirectoryKey          key = iota
	appManagementProcessRepositoryKey key = iota
	organizationLogoUrlKey            key = iota
	loginURLKey                       key = iota
	callbackURLKey                    key = iota
	loggerKey                         key = iota
	digidProxyURLKey                  key = iota
	digidProxyPublicKeyKey            key = iota
)

type RouterConfig struct {
	LogoURL                        string
	AppManagementProcessRepository repositories.AppManagementProcessRepository
	HTMLTemplateDirectory          string
	LoginURL                       string
	CallbackURL                    string
	DigidProxyURL                  string
	DigidProxyPublicKey            string
}

func NewRouter(config RouterConfig, logger *zap.Logger) *chi.Mux {
	r := chi.NewRouter()

	metricsMiddleware := metrics.NewMiddleware("app-management-ui")
	r.Use(metricsMiddleware)
	r.Handle("/metrics", promhttp.Handler())

	r.Route("/login", func(r chi.Router) {
		r.Use(middleware.Logger)
		r.Get("/", func(responseWriter http.ResponseWriter, request *http.Request) {
			ctx := context.WithValue(request.Context(), htmlTemplateDirectoryKey, config.HTMLTemplateDirectory)
			ctx = context.WithValue(ctx, organizationLogoUrlKey, config.LogoURL)
			ctx = context.WithValue(ctx, loginURLKey, config.LoginURL)
			ctx = context.WithValue(ctx, callbackURLKey, config.CallbackURL)
			ctx = context.WithValue(ctx, loggerKey, logger)
			ctx = context.WithValue(ctx, digidProxyURLKey, config.DigidProxyURL)
			handlerLoginPage(responseWriter, request.WithContext(ctx))
		})
	})

	r.Route("/activate", func(r chi.Router) {
		r.Use(middleware.Logger)
		r.Get("/", func(responseWriter http.ResponseWriter, request *http.Request) {
			ctx := context.WithValue(request.Context(), htmlTemplateDirectoryKey, config.HTMLTemplateDirectory)
			ctx = context.WithValue(ctx, appManagementProcessRepositoryKey, config.AppManagementProcessRepository)
			ctx = context.WithValue(ctx, organizationLogoUrlKey, config.LogoURL)
			ctx = context.WithValue(ctx, loginURLKey, config.LoginURL)
			ctx = context.WithValue(ctx, loggerKey, logger)
			handlerActivatePage(responseWriter, request.WithContext(ctx))
		})
	})

	r.Route("/activate_digid", func(r chi.Router) {
		r.Use(middleware.Logger)
		r.Get("/", func(responseWriter http.ResponseWriter, request *http.Request) {
			ctx := context.WithValue(request.Context(), htmlTemplateDirectoryKey, config.HTMLTemplateDirectory)
			ctx = context.WithValue(ctx, appManagementProcessRepositoryKey, config.AppManagementProcessRepository)
			ctx = context.WithValue(ctx, organizationLogoUrlKey, config.LogoURL)
			ctx = context.WithValue(ctx, loginURLKey, config.LoginURL)
			ctx = context.WithValue(ctx, loggerKey, logger)
			ctx = context.WithValue(ctx, digidProxyPublicKeyKey, config.DigidProxyPublicKey)
			handlerActivateDigidPage(responseWriter, request.WithContext(ctx))
		})
	})

	healthCheckHandler := healthcheck.NewHandler("app-management-ui", []healthcheck.Checker{})
	r.Route("/health", func(r chi.Router) {
		r.Get("/", healthCheckHandler.HandleHealth)
		r.Get("/check", healthCheckHandler.HandlerHealthCheck)
	})

	return r
}
