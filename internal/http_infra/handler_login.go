// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"html/template"
	"net/http"
	"path"
	"path/filepath"
	"time"

	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-ui/pkg/events"
)

type performLoginPage struct {
	LogoURL       string
	LoginURL      string
	DigidProxyURL string
}

func handlerLoginPage(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger := context.Value(loggerKey).(*zap.Logger)
	templateDirectory, _ := context.Value(htmlTemplateDirectoryKey).(string)
	organizationLogoURL, _ := context.Value(organizationLogoUrlKey).(string)
	loginURL, _ := context.Value(loginURLKey).(string)
	callbackURL, _ := context.Value(callbackURLKey).(string)
	digidProxyUrl, _ := context.Value(digidProxyURLKey).(string)

	event := events.AMUI_3
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	registrationToken := request.URL.Query().Get("registrationToken")
	if registrationToken == "" {
		event = events.AMUI_4
		logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
		http.Error(responseWriter, "no registrationToken provided", http.StatusBadRequest)
		return
	}

	templatePath := filepath.Join(templateDirectory, "login.html")
	filePath := path.Join(templatePath)

	parsedTemplate, err := template.ParseFiles(filePath)
	if err != nil {
		event = events.AMUI_5
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "the page could not be loaded", http.StatusInternalServerError)
		return
	}

	expiration := time.Now().Add(10 * time.Minute)
	cookie := http.Cookie{Name: "registrationToken", Value: registrationToken, Expires: expiration, SameSite: http.SameSiteLaxMode}
	http.SetCookie(responseWriter, &cookie)

	err = parsedTemplate.Execute(responseWriter, &performLoginPage{
		LogoURL:       organizationLogoURL,
		LoginURL:      loginURL + "?callbackURL=" + callbackURL + "/activate",
		DigidProxyURL: digidProxyUrl,
	})

	if err != nil {
		event = events.AMUI_6
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "the page could not be loaded", http.StatusInternalServerError)
		return
	}
	event = events.AMUI_7
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}
