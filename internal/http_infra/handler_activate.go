// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"encoding/base64"
	"html/template"
	"net/http"
	"path"
	"path/filepath"
	"strings"

	"github.com/golang-jwt/jwt/v5"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/pkg/model"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/pkg/repositories"
	serviceModel "gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-service/pkg/model"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-ui/pkg/events"
)

type performActivatePage struct {
	LogoURL            string //TODO: Delete organization logo url
	AppURL             template.URL
	GivenName          string
	FamilyName         string
	BirthdateFormatted string
	Bsn                string
}

type userInfo struct {
	givenName  string
	familyName string
	birthDate  string
	bsn        string
}

func handlerActivatePage(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger := context.Value(loggerKey).(*zap.Logger)

	appManagementProcessRepository, _ := context.Value(appManagementProcessRepositoryKey).(repositories.AppManagementProcessRepository)
	templateDirectory, _ := context.Value(htmlTemplateDirectoryKey).(string)
	organizationLogoURL, _ := context.Value(organizationLogoUrlKey).(string)

	event := events.AMUI_8
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	bsn := request.URL.Query().Get("bsn")
	if bsn == "" {
		event = events.AMUI_9
		logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
		http.Error(responseWriter, "bsn required", http.StatusBadRequest)
		return
	}

	registrationTokenCookie, err := request.Cookie("registrationToken")
	if err != nil {
		event = events.AMUI_10
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "registrationToken registrationTokenCookie is missing or expired - the page could not be loaded", http.StatusInternalServerError)
		return
	}

	registration, err := appManagementProcessRepository.LinkUserIdentity(registrationTokenCookie.Value, model.UserIdentity{Bsn: bsn})
	if err != nil {
		event = events.AMUI_11
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "the page could not be loaded", http.StatusInternalServerError)
		return
	}

	templatePath := filepath.Join(templateDirectory, "activate.html")
	filePath := path.Join(templatePath)

	parsedTemplate, err := template.ParseFiles(filePath)
	if err != nil {
		event = events.AMUI_12
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "the page could not be loaded", http.StatusInternalServerError)
		return
	}

	user := getUser(bsn)
	returnUrl := getReturnUrl(registration)

	err = parsedTemplate.Execute(responseWriter, &performActivatePage{
		LogoURL:            organizationLogoURL,
		AppURL:             template.URL(returnUrl),
		GivenName:          user.givenName,
		FamilyName:         user.familyName,
		BirthdateFormatted: user.birthDate,
		Bsn:                user.bsn,
	})

	if err != nil {
		event = events.AMUI_14
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "the page could not be loaded", http.StatusInternalServerError)
		return
	}
	event = events.AMUI_15
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerActivateDigidPage(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger := context.Value(loggerKey).(*zap.Logger)

	appManagementProcessRepository, _ := context.Value(appManagementProcessRepositoryKey).(repositories.AppManagementProcessRepository)
	templateDirectory, _ := context.Value(htmlTemplateDirectoryKey).(string)
	organizationLogoURL, _ := context.Value(organizationLogoUrlKey).(string)
	digilabProxyKeyEncoded, _ := context.Value(digidProxyPublicKeyKey).(string)
	digidProxyKey, _ := base64.StdEncoding.DecodeString(digilabProxyKeyEncoded)

	event := events.AMUI_8
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	bsnJwtToken := request.URL.Query().Get("token")

	if bsnJwtToken == "" {
		event = events.AMUI_18
		logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
		http.Error(responseWriter, "bsn JWT required", http.StatusBadRequest)
		return
	}

	parser := jwt.NewParser()
	unverifiedBsnToken, err := parser.Parse(bsnJwtToken, func(token *jwt.Token) (interface{}, error) {
		return jwt.ParseRSAPublicKeyFromPEM(digidProxyKey)
	})
	if err != nil {
		event = events.AMUI_17
		logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
		http.Error(responseWriter, "bsn jwt unparseable", http.StatusBadRequest)
		return
	}

	bsnSubject := unverifiedBsnToken.Claims.(jwt.MapClaims)["sub"].(string) //s00000000:111222333
	bsn := strings.Split(bsnSubject, ":")[1]
	if bsn == "" {
		event = events.AMUI_9
		logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
		http.Error(responseWriter, "bsn required", http.StatusBadRequest)
		return
	}

	registrationTokenCookie, err := request.Cookie("registrationToken")
	if err != nil {
		event = events.AMUI_10
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "registrationToken registrationTokenCookie is missing or expired - the page could not be loaded", http.StatusInternalServerError)
		return
	}

	registration, err := appManagementProcessRepository.LinkUserIdentity(registrationTokenCookie.Value, model.UserIdentity{Bsn: bsn})
	if err != nil {
		event = events.AMUI_11
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "the page could not be loaded", http.StatusInternalServerError)
		return
	}

	templatePath := filepath.Join(templateDirectory, "activate.html")
	filePath := path.Join(templatePath)

	parsedTemplate, err := template.ParseFiles(filePath)
	if err != nil {
		event = events.AMUI_12
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "the page could not be loaded", http.StatusInternalServerError)
		return
	}

	user := getUser(bsn)
	returnUrl := getReturnUrl(registration)

	err = parsedTemplate.Execute(responseWriter, &performActivatePage{
		LogoURL:            organizationLogoURL,
		AppURL:             template.URL(returnUrl),
		GivenName:          user.givenName,
		FamilyName:         user.familyName,
		BirthdateFormatted: user.birthDate,
		Bsn:                user.bsn,
	})
	if err != nil {
		event = events.AMUI_14
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "the page could not be loaded", http.StatusInternalServerError)
		return
	}

	event = events.AMUI_15
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func getUser(bsn string) userInfo {
	var user userInfo

	switch bsn {
	case "814859094":
		user.givenName = "Piet"
		user.familyName = "Verstraten"
		user.birthDate = "10 maart 1995"
	case "309777938":
		user.givenName = "Barbara"
		user.familyName = "Goulouse"
		user.birthDate = "15 mei 1951"
	case "950053545":
		user.givenName = "Çigdem"
		user.familyName = "Kemal"
		user.birthDate = "12 januari 2002"
	case "761582915":
		user.givenName = "Hendrik-jan"
		user.familyName = "Jansens"
		user.birthDate = "9 december 1982"
	case "570957588":
		user.givenName = "Lance"
		user.familyName = "Smit"
		user.birthDate = "30 april 1971"
	case "999992740":
		user.givenName = "Jael"
		user.familyName = "de Jager"
		user.birthDate = "1 januari 2000"
	default:
		user.givenName = "Henk"
		user.familyName = "Smit"
		user.birthDate = "30 april 1971"
	}
	user.bsn = bsn

	return user
}

func getReturnUrl(registration *serviceModel.Registration) string {
	var returnUrl string
	switch registration.ClientId {
	case "development_web":
		returnUrl = "http://localhost:49430/#/activatie-voltooien"
	case "test_web":
		returnUrl = "https://app.vorijk-test.blauweknop.app/#/activatie-voltooien"
	case "demo_web":
		returnUrl = "https://demo.vorijk.nl/#/activatie-voltooien"
	case "local_web":
		returnUrl = "http://user-ui.citizen.vorderingenoverzicht.blauweknop.bk/#/activatie-voltooien"
	case "native_app":
		returnUrl = "app://vorijk.nl/activatie-voltooien"
	}
	return returnUrl
}
