FROM golang:1.23.1-alpine3.20 AS build

RUN apk add --update --no-cache git

COPY ./cmd /go/src/app-management-ui/cmd
COPY ./html /go/src/app-management-ui/html
COPY ./internal /go/src/app-management-ui/internal
COPY ./pkg /go/src/app-management-ui/pkg
COPY ./go.mod /go/src/app-management-ui/
COPY ./go.sum /go/src/app-management-ui/
WORKDIR /go/src/app-management-ui
RUN go mod download \
 && go build -o dist/bin/app-management-ui ./cmd/app-management-ui

# Release binary on latest alpine image.
FROM alpine:3.20

ARG USER_ID=10001
ARG GROUP_ID=10001

COPY --from=build /go/src/app-management-ui/dist/bin/app-management-ui /usr/local/bin/app-management-ui
COPY --from=build /go/src/app-management-ui/html/templates/login.html /html/templates/login.html
COPY --from=build /go/src/app-management-ui/html/templates/activate.html /html/templates/activate.html

# Add non-priveleged user. Disabled for openshift
RUN addgroup -g "$GROUP_ID" appgroup \
 && adduser -D -u "$USER_ID" -G appgroup appuser
USER appuser
HEALTHCHECK none
CMD ["/usr/local/bin/app-management-ui"]
