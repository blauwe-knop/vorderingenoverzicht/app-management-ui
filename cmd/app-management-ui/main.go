// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package main

import (
	"errors"
	"fmt"
	"log"
	"net/http"

	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/pkg/repositories"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-ui/internal/http_infra"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-ui/pkg/events"
)

type options struct {
	ListenAddress               string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:80" description:"Address for the app-management-ui api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	AppManagementProcessAddress string `long:"app-management-process-address" env:"APP_MANAGEMENT_PROCESS_ADDRESS" default:"http://localhost:80" description:"app management process address."`
	OrganizationLogoURL         string `long:"organization-logo-url" env:"ORGANIZATION_LOGO_URL" description:"Organization logo URL"`
	LoginURL                    string `long:"login-url" env:"LOGIN_URL" description:"Login url"`
	CallbackURL                 string `long:"callback-url" env:"CALLBACK_URL" description:"Callback url"`
	DigidProxyURL               string `long:"digid-proxy-url" env:"DIGID_PROXY_URL" description:"Digid proxy url"`
	DigidProxyPublicKey         string `long:"digid-proxy-publickey" env:"DIGID_PROXY_PUBLICKEY" description:"Digid proxy publickey"`
	LogOptions
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	logger, err := newLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	appManagementProcess := repositories.NewAppManagementProcessClient(cliOptions.AppManagementProcessAddress)

	routerConfig := http_infra.RouterConfig{
		AppManagementProcessRepository: appManagementProcess,
		LogoURL:                        cliOptions.OrganizationLogoURL,
		LoginURL:                       cliOptions.LoginURL,
		CallbackURL:                    cliOptions.CallbackURL,
		DigidProxyURL:                  cliOptions.DigidProxyURL,
		DigidProxyPublicKey:            cliOptions.DigidProxyPublicKey,
		HTMLTemplateDirectory:          "html/templates/",
	}

	router := http_infra.NewRouter(routerConfig, logger)

	event := events.AMUI_1
	logger.Log(event.GetLogLevel(), event.Message, zap.String("listenAddress", cliOptions.ListenAddress), zap.Reflect("event", event))
	err = http.ListenAndServe(cliOptions.ListenAddress, router)
	if err != nil {
		event = events.AMUI_2
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		if !errors.Is(err, http.ErrServerClosed) {
			panic(err)
		}
	}
}

func newLogger(logOptions LogOptions) (*zap.Logger, error) {
	config := logOptions.ZapConfig()
	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return logger, nil
}
